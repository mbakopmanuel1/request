var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');

var users = require('./routes/users');
var facultes = require('./routes/facultes');
var departements = require('./routes/departements');
var filieres = require('./routes/filieres');
var niveaux = require('./routes/niveaux');
var etudiants = require('./routes/etudiants');
var ues = require('./routes/ues');
var annees = require('./routes/annees');
var responsables = require('./routes/responsables');
var grades = require('./routes/grades');
var motifs = require('./routes/motifs');
var requetes = require('./routes/requetes');
var galleries = require('./routes/galleries');
var statusrequetes = require('./routes/statusrequetes');
var notificationRequetes = require('./routes/notificationEtudiants');
var app = express();

var mongoose = require('mongoose');

mongoose.Promise = require('bluebird');
mongoose.connect('mongodb://localhost/RequestSend', { promiseLibrary: require('bluebird') })
  .then(() =>  console.log('connecté sans erreur'))
  .catch((err) => console.error(err));


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended':'false'}));
app.use(express.static(path.join(__dirname, 'dist')));


app.use('/Users', express.static(path.join(__dirname, 'dist')));
app.use('/user', users);
app.use('/Facultes', express.static(path.join(__dirname, 'dist')));
app.use('/faculte', facultes);
app.use('/Departements', express.static(path.join(__dirname, 'dist')));
app.use('/departement', departements);
app.use('/Filieres', express.static(path.join(__dirname, 'dist')));
app.use('/filiere', filieres);
app.use('/Niveaux', express.static(path.join(__dirname, 'dist')));
app.use('/niveau', niveaux);
app.use('/Etudiants', express.static(path.join(__dirname, 'dist')));
app.use('/etudiant', etudiants);
app.use('/Ues', express.static(path.join(__dirname, 'dist')));
app.use('/ue', ues);
app.use('/Annees', express.static(path.join(__dirname, 'dist')));
app.use('/annee', annees);
app.use('/Responsables', express.static(path.join(__dirname, 'dist')));
app.use('/responsable', responsables);
app.use('/Grades', express.static(path.join(__dirname, 'dist')));
app.use('/grade', grades);
app.use('/Galleries', express.static(path.join(__dirname, 'dist')));
app.use('/gallerie', galleries);
app.use('/Motifs', express.static(path.join(__dirname, 'dist')));
app.use('/motif', motifs);
app.use('/StatusRequetes', express.static(path.join(__dirname, 'dist')));
app.use('/statusrequete', statusrequetes);
app.use('/Requetes', express.static(path.join(__dirname, 'dist')));
app.use('/requete', requetes);
app.use('/notificationRequetes', express.static(path.join(__dirname, 'dist')));
app.use('/notificationrequete', requetes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


app.use(function(req, res, next) {
		res.header("Access-Control-Allow-Origin", "*");
		res.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
		next();
});



module.exports = app;