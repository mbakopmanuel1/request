import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule,JsonpModule, Jsonp, Response } from '@angular/http';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app.routing';
import { NavbarModule } from './shared/navbar/navbar.module';
import { FooterModule } from './shared/footer/footer.module';
import { SidebarModule } from './sidebar/sidebar.module';

import { AppComponent } from './app.component';

//services
import { UsersService} from './services/user-service/users.service';
import { ServiceRequeteService} from './services/requete-service/service-requete.service';

//components
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { UsersComponent } from './users/users.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { RequeteEnCoursComponent } from './requete-en-cours/requete-en-cours.component';
import { RequeteAboutisComponent } from './requete-aboutis/requete-aboutis.component';
import { RequeteEchouaComponent } from './requete-echoua/requete-echoua.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UserComponent,
    UsersComponent,
    NotificationsComponent,
    RequeteEnCoursComponent,
    RequeteAboutisComponent,
    RequeteEchouaComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NavbarModule,
    FooterModule,
    SidebarModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule,
    JsonpModule
  ],
  providers: [
    UsersService,
    ServiceRequeteService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
