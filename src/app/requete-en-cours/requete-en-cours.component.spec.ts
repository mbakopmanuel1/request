import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequeteEnCoursComponent } from './requete-en-cours.component';

describe('RequeteEnCoursComponent', () => {
  let component: RequeteEnCoursComponent;
  let fixture: ComponentFixture<RequeteEnCoursComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequeteEnCoursComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequeteEnCoursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
