import { Component, OnInit } from '@angular/core';

import { ServiceRequeteService } from '../services/requete-service/service-requete.service';
@Component({
  selector: 'app-requete-en-cours',
  templateUrl: './requete-en-cours.component.html',
  styleUrls: ['./requete-en-cours.component.scss']
})
export class RequeteEnCoursComponent implements OnInit {

	requetesEnCour:any;
	requetesAboutis:any;
	requetesRejettees:any;

	requetesEnCourLength:number;
	requetesAboutisLength:number;
	requetesRejetteesLength:number;

  constructor(private serviceRequete:ServiceRequeteService) { }

  ngOnInit() {

  		this.requetesEnCour = this.serviceRequete.getAllEnCours();
  		this.requetesAboutis = this.serviceRequete.getAllAboutis();
  		this.requetesRejettees = this.serviceRequete.getAllRejettées();


  		this.requetesEnCourLength = this.requetesEnCour.length();
  		this.requetesAboutisLength = this.requetesAboutis.length();
  		this.requetesRejetteesLength = this.requetesRejettees.length();
  }

}
