import { Component, OnInit } from '@angular/core';
import { JsonPipe } from '@angular/common';
import { UsersService } from '../services/user-service/users.service';
import { Users } from '../class/user-class/users';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

	users : any;

  constructor(private usersService: UsersService) {
    this.users = this.usersService.getAllUsers();
    console.log(this.users);
    console.log(JSON.stringify(this.users));
   }

  ngOnInit() {

  	this.users = this.usersService.getAllUsers();
  	console.log(this.users);
  	console.log(JSON.stringify(this.users));
  }

}
