import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { RequeteAboutisComponent } from './requete-aboutis/requete-aboutis.component';
import { RequeteEchouaComponent } from './requete-echoua/requete-echoua.component';
import { RequeteEnCoursComponent } from './requete-en-cours/requete-en-cours.component';

const routes: Routes =[
    { path: 'dashboard',      component: HomeComponent },
    { path: 'en cours',  component: RequeteEnCoursComponent },
    { path: 'abouties',  component: RequeteAboutisComponent },
    { path: 'rejettés',  component: RequeteEchouaComponent },
    { path: 'user',           component: UserComponent },
    { path: 'notification',           component: NotificationsComponent },
      { path: '',          redirectTo: 'dashboard', pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
