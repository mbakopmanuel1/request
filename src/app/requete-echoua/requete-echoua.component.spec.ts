import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequeteEchouaComponent } from './requete-echoua.component';

describe('RequeteEchouaComponent', () => {
  let component: RequeteEchouaComponent;
  let fixture: ComponentFixture<RequeteEchouaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequeteEchouaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequeteEchouaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
