import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {JsonpModule, Jsonp, Response, } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { ajax } from 'rxjs/observable/dom/ajax';
import { map, catchError } from 'rxjs/operators';
import 'rxjs/add/operator/map';

const apiUrl = "/user";

@Injectable()
export class UsersService {

	users : any;
  constructor(private http : HttpClient,private jsonp : Jsonp) { }

  getAllUsers(){

  this.http.get<JSON>('/user').subscribe(
          data =>{
            this.users = data;
          }
        );
  	return this.users;
  }

}
