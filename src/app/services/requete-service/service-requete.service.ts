import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {JsonpModule, Jsonp, Response, } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { ajax } from 'rxjs/observable/dom/ajax';
import { map, catchError } from 'rxjs/operators';
import 'rxjs/add/operator/map';

const apiUrl = "/requete";
@Injectable()
export class ServiceRequeteService {

	requetes: any;

  constructor(private http : HttpClient,private jsonp : Jsonp) { }

  getAllEnCours():any{

  		this.http.get<JSON>(apiUrl+"/en cours")
  				.subscribe(data=>{
  					this.requetes = data;
  				}),
  				error=>{
  					this.requetes = error;
  				}

  		return this.requetes;
  }

  getAllAboutis():any{

  		this.http.get<JSON>(apiUrl+"/en cours")
  				.subscribe(data=>{
  					this.requetes = data;
  				}),
  				error=>{
  					this.requetes = error;
  				}

  		return this.requetes;
  }

  getAllRejettées():any{

  		this.http.get<JSON>(apiUrl+"/en cours")
  				.subscribe(data=>{
  					this.requetes = data;
  				}),
  				error=>{
  					this.requetes = error;
  				}

  		return this.requetes;
  }

}
