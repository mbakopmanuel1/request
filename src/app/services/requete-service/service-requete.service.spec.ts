import { TestBed, inject } from '@angular/core/testing';

import { ServiceRequeteService } from './service-requete.service';

describe('ServiceRequeteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ServiceRequeteService]
    });
  });

  it('should be created', inject([ServiceRequeteService], (service: ServiceRequeteService) => {
    expect(service).toBeTruthy();
  }));
});
