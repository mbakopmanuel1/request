var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var annees = require('../models/annees');
var responsable = require('../models/responsables');
var ue = require('../models/ues');

/* GET home page. */
router.get('/', function(req, res, next) {
  annees.find()
  		.then((an)=>{
  			res.json(an);
  		})
});

// Post
router.post('/',function(req,res,next){
	responsable.find({nom: req.query.responsable})
				.then((respo)=>{
					ue.find({libelle: {$in:req.query.ue}})
						.then((ues)=>{

							var an = new annees({

								annee: req.query.annee,
								dispenser:[]
							});

							an.save()
								.then((safe)=>{
									safe.update({
										$push:{
											dispenser:{
												responsable: respo[0],
												ues: ues
											}
										}
									})
									.then((updates)=>{
										res.json(updates);
									})
								})
						})
				})
});

module.exports = router;