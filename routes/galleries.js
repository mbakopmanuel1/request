var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var galleries = require('../models/galleries');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send('galleries');
});

// Post
router.post('/',function(req,res,next){
	galleries.create(req.query,function(err,post){
		if(err) return next(err);
		res.json(post);
	});
});

module.exports = router;