var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var niveaux = require('../models/niveaux');

/* GET home page. */
router.get('/', function(req, res, next) {
  niveaux.find()
  		 .then((niveau)=>{
  		 	res.json(niveau);
  		 });
});

// Post
router.post('/',function(req,res,next){
	niveaux.create(req.query,function(err,post){
		if(err) return next(err);
		res.json(post);
	});
});

module.exports = router;