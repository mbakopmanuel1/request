var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var etudiants = require('../models/etudiants');
var filiere = require('../models/filieres');
var ues = require('../models/ues');
var annee = require('../models/annees');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send('etudiants');
});

// Post
router.post('/',function(req,res,next){

	filiere.find({nom: req.query.filiere})
			.then((fil)=>{

				ues.find({libelle:{$in: req.query.ues}})
					.where('filiere')
					.equals(fil[0])
					.then((ue)=>{

						annee.find({annee: req.query.annee})
							.then((an)=>{


								var etud = new etudiants({
									nom: req.query.nom,
									prenom: req.query.prenom,
									matricule: req.matricule,
									filiere: fil[0],
									inscription: [{
										ues: ue,
										annee: an[0],
									}]
								});

								etud.save()
									.then((ok)=>{
										res.json(ok);
									})
							})
					});
				
			});
});

module.exports = router;