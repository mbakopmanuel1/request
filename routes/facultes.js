var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var facultes = require('../models/facultes');

/* GET home page. */
router.get('/', function(req, res, next) {
  facultes.find(function(err,facultes){
  		if(err) return next(err);
  		res.json(facultes);
  	})
});

// Post
router.post('/',function(req,res,next){
	facultes.create(req.query,function(err,post){
		if(err) return next(err);
		res.json(post);
	});
});

//get nom
router.get('/:nom', function(req, res, next) {
	console.log(req.params.nom);
  	facultes.find({nom:req.params.nom}, function(err,facultes){
  		if(err) return next(err);
  		res.json(facultes);
  		return facultes;
  	})
});

module.exports = router;