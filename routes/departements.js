var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var departements = require('../models/departementModel');
var facultes = require('../models/facultes');

/* GET home page. */
router.get('/', function(req, res, next) {
  departements.find()
              .populate('faculte').then((deps)=>{
                   res.json(deps);
                   return deps;
              });
});

// Post
router.post('/',function(req,res,next){
    facultes.find({nom:req.query.faculte}, function(err,facult){
      if(err) return next(err);    
      var departement = new departements({nom:req.query.nom,faculte:facult[0]});
      departement.save().then(()=>{

        res.json(departement.getMe());
      })
      
    })
	
});

module.exports = router;