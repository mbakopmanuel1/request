var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var notificationRequetes = require('../models/notificationEtudiants');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send('notifs');
});

// Post
router.post('/',function(req,res,next){
	notificationRequetes.create(req.query,function(err,post){
		if(err) return next(err);
		res.json(post);
	});
});

module.exports = router;