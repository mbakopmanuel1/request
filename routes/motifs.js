var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var motifs = require('../models/motifs');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send('motifs');
});

// Post
router.post('/',function(req,res,next){
	motifs.create(req.query,function(err,post){
		if(err) return next(err);
		res.json(post);
	});
});

module.exports = router;