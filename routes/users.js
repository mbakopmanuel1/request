var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var users = require('../models/users');

/* GET home page. */
router.get('/', function(req, res, next) {
  	users.find(function(err,users){
  		if(err) return next(err);
  		res.json(users);
  	})
});

/* GET home page single. */
router.get('/:id', function(req, res, next) {
	console.log(req.params.id);
  	users.find({_id:req.params.id}, function(err,users){
  		if(err) return next(err);
  		res.json(users);
  	})
});

router.post('/',function(req,res,next){

	users.create(req.query,function(err,post){
		if(err) return next(err);
		res.json(post);
	});
});

module.exports = router;