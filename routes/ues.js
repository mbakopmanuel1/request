var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var ues = require('../models/ues');
var annee = require('../models/annees');
var responsable = require('../models/responsables');
var filiere = require('../models/filieres');
var niveau = require('../models/niveaux');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send('ues');
});

// Post
router.post('/',function(req,res,next){
	filiere.find({nom:req.query.filiere})
			.then((filier)=>{

				niveau.find({nom: req.query.niveau})
						.then((niveaux)=>{

							var ue = new ues({
									libelle:req.query.libelle,
									credit:req.query.credit,
									filiere:filier[0],
									niveau:niveaux[0],
								});

								ue.save()
									.then((ok)=>{
										res.json(ok);						
									});
							});
						})
				
	
});

module.exports = router;