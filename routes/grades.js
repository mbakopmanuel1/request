var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var grades = require('../models/grades');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send('grades');
});

// Post
router.post('/',function(req,res,next){
	grades.create(req.query,function(err,post){
		if(err) return next(err);
		res.json(post);
	});
});

module.exports = router;