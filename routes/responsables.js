var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var responsables = require('../models/responsables');
var grades = require('../models/grades');

/* GET home page. */
router.get('/', function(req, res, next) {
  responsables.find()
  				.populate('grade')
  				.then((responsable)=>{
  					res.json(responsable);
  				})
});

// Post
router.post('/',function(req,res,next){

	grades.find({libelle:req.query.grade})
			.then((grade)=>{

				var responsable = new responsables({
													nom:req.query.nom,
													prenom:req.query.prenom,
													specialite:req.query.specialite,
													grade:grade[0]
													});
				responsable.save()
							.then((respo)=>{
								res.json(respo);
							});

			});
	responsables.create(req.query,function(err,post){
		if(err) return next(err);
		res.json(post);
	});
});

module.exports = router;