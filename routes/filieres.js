var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var filieres = require('../models/filieres');
var departements = require('../models/departementModel');
var niveaux = require('../models/niveaux');
/* GET home page. */
router.get('/', function(req, res, next) {
  filieres.find()
          .populate('departement')
          .populate('niveaux')
  		  .then((filiere) => {

  		  	res.json(filiere);
  		  	return filiere;
  		  })
});

// Post
router.post('/',function(req,res,next){
	
	  departements.find({nom:req.query.departement})
              .populate('faculte').then((deps)=>{
                     niveaux.find({nom: {$in:req.query.niveaux}})
                                      .then((fil)=>{
                                            var filiere = new filieres({nom:req.query.nom,departement:deps[0],niveaux:fil});
                                            filiere.save()
                                                    .then((filier)=>{
                                                      res.json(filier);
                                                    })
                                      });
                   
              });
});

module.exports = router;