var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var requetes = require('../models/requetes');
var etudiants = require('../models/etudiants');
var motifs = require('../models/motifs');
var ues = require('../models/ues');
var status = require('../models/statusrequetes');
var pieces_jointes = require('../models/galleries');
var destinataire = require('../models/responsables');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send('requetes');
});

// Post
router.post('/',function(req,res,next){
	
	etudiants.find({nom:req.query.etudiant})
			.then((etudiant)=>{

				destinataire.find({nom: req.query.destinataire})
						.then((dest)=>{

							motifs.find({libelle: req.query.motif})
								.then((motif)=>{

									ues.find({libelle: req.query.ue})
										.then((ue)=>{
											pieces_jointes.find({chemin:{$in:req.query.pieces}})
															.then((piece)=>{

																status.find({libelle:req.query.status})
																		.then((statu)=>{

																			var requet = new requetes({
																				objet:req.query.objet,
																				description: req.query.description,
																				etudiant: etudiant[0],
																				destinataire: dest[0],
																				motif: motif[0],
																				ue: ue[0],
																				pieces_jointes: piece,
																				status: statu[0]
																			});
																			requet.save()
																					.then((reqq)=>{
																							res.json(reqq);
																					})
																		})
															})
										})
								})
						})
			})
});


router.get('/:status', function(req, res, next) {
  status.find({libelle:req.params.status})
  		.then((stat)=>{
  			requetes.find()
  					.where('status').equals(stat[0])
  					.populate('destinataire')
  					.then((requete)=>{
  						res.json(requete);
  					})
  		})
});

module.exports = router;