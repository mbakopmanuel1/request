var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var statusrequetes = require('../models/statusrequetes');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send('statusrequetes');
});

// Post
router.post('/',function(req,res,next){
	statusrequetes.create(req.query,function(err,post){
		if(err) return next(err);
		res.json(post);
	});
});

module.exports = router;