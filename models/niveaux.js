var mongoose = require('mongoose');


var niveaux = new mongoose.Schema({

	nom: String,
	created_at: { type: Date, default: Date.now},
	updated_at: { type: Date, default: Date.now},
	deleted_at: { type: Date, default: Date.now},

});

module.exports = mongoose.model('niveaux',niveaux);