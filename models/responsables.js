var mongoose = require('mongoose');


var responsables = new mongoose.Schema({

	nom: String,
	prenom: String,
	specialite: String,
	grade: {type: mongoose.Schema.Types.ObjectId, ref:'grades'},
	transfert: [{requetes: {type:mongoose.Schema.Types.ObjectId, ref: 'requetes'}, destinataire: {type:mongoose.Schema.Types.ObjectId, ref: 'responsables'} }],
	created_at: { type: Date, default: Date.now},
	updated_at: { type: Date, default: Date.now},
	deleted_at: { type: Date, default: Date.now},

});

module.exports = mongoose.model('responsables',responsables);