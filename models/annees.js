var mongoose = require('mongoose');


var annees = new mongoose.Schema({

	annee: String,
	dispenser:[{responsable:{type: mongoose.Schema.Types.ObjectId, ref:'responsables'}, ues:[{type: mongoose.Schema.Types.ObjectId, ref:'ues'}]}],
	created_at: { type: Date, default: Date.now},
	updated_at: { type: Date, default: Date.now},
	deleted_at: { type: Date, default: Date.now},

});

module.exports = mongoose.model('annees',annees);