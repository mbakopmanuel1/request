var mongoose = require('mongoose');


var motifs = new mongoose.Schema({

	libelle: String,
	created_at: { type: Date, default: Date.now},
	updated_at: { type: Date, default: Date.now},
	deleted_at: { type: Date, default: Date.now},

});

module.exports = mongoose.model('motifs',motifs);