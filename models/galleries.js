var mongoose = require('mongoose');


var galleries = new mongoose.Schema({

	chemin: String,
	created_at: { type: Date, default: Date.now},
	updated_at: { type: Date, default: Date.now},
	deleted_at: { type: Date, default: Date.now},

});

module.exports = mongoose.model('galleries',galleries);