var mongoose = require('mongoose');


var ues = new mongoose.Schema({

	libelle: String,
	credit: Number,
	filiere:{type:mongoose.Schema.Types.ObjectId, ref: 'filieres'},
	niveau:{type:mongoose.Schema.Types.ObjectId, ref: 'niveaux'},
	created_at: { type: Date, default: Date.now},
	updated_at: { type: Date, default: Date.now},
	deleted_at: { type: Date, default: Date.now},

});

module.exports = mongoose.model('ues',ues);
