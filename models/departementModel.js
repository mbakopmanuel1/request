var mongoose = require('mongoose');

var departement = new mongoose.Schema({

	nom: String,
	faculte: {type:mongoose.Schema.Types.ObjectId, ref:'facultes'},
	created_at: { type: Date, default: Date.now},
	updated_at: { type: Date, default: Date.now},
	deleted_at: { type: Date, default: Date.now},

});

departement.methods.getMe = function(){
	return {
			_id:this._id,
			nom:this.nom,
			faculte: this.faculte,
			created_at: this.created_at,
			updated_at: this.updated_at,
			deleted_at: this.deleted_at,
		}
};


module.exports = mongoose.model('departements',departement);