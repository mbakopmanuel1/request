var mongoose = require('mongoose');


var facultes = new mongoose.Schema({

	nom: String,
	created_at: { type: Date, default: Date.now},
	updated_at: { type: Date, default: Date.now},
	deleted_at: { type: Date, default: Date.now},

});


facultes.methods.getMe = function(){
	return {
			_id:this._id,
			nom:this.nom,
			created_at: this.created_at,
			updated_at: this.updated_at,
			deleted_at: this.deleted_at,
		}
};

module.exports = mongoose.model('facultes',facultes);