var mongoose = require('mongoose');

var filieres = new mongoose.Schema({

	nom: {type: String, index:{unique:true}},
	departement: {type:mongoose.Schema.Types.ObjectId, ref:'departements'},
	niveaux:[{type:mongoose.Schema.Types.ObjectId, ref:'niveaux'}],
	created_at: { type: Date, default: Date.now},
	updated_at: { type: Date, default: Date.now},
	deleted_at: { type: Date, default: Date.now},

});

filieres.methods.getMe = function(){
	return {
			_id:this._id,
			nom:this.nom,
			departement: this.departement,
			created_at: this.created_at,
			updated_at: this.updated_at,
			deleted_at: this.deleted_at,
		}
};

module.exports = mongoose.model('filieres',filieres);