var mongoose = require('mongoose');


var notificationRequetes = new mongoose.Schema({

	libelle: String,
	requete: {type:mongoose.Schema.ObjectId, ref: 'requetes'},
	etudiant: {type:mongoose.Schema.ObjectId, ref: 'etudiants'},
	//status: {type:mongoose.Schema.ObjectId, ref: 'statusNotifications'},
	created_at: { type: Date, default: Date.now},
	updated_at: { type: Date, default: Date.now},
	deleted_at: { type: Date, default: Date.now},

});

module.exports = mongoose.model('notificationEtudiants',notificationRequetes);