var mongoose = require('mongoose');


var etudiants = new mongoose.Schema({

	nom: String,
	prenom: String,
	matricule: String,
	filiere: { type:mongoose.Schema.Types.ObjectId, ref: 'filieres'},
	inscription: [{ues:[{type:mongoose.Schema.Types.ObjectId, ref:'ues'}], annee: {type:mongoose.Schema.Types.ObjectId, ref:'annees'}}],
	created_at: { type: Date, default: Date.now},
	updated_at: { type: Date, default: Date.now},
	deleted_at: { type: Date, default: Date.now},

});

module.exports = mongoose.model('etudiants',etudiants);