var mongoose = require('mongoose');


var users = new mongoose.Schema({

	nom: String,
	prenom: String,
	password:{type: String, require:true},
	created_at: { type: Date, default: Date.now},
	updated_at: { type: Date, default: Date.now},
	deleted_at: { type: Date, default: Date.now},

});

module.exports = mongoose.model('Users',users);