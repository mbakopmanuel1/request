var mongoose = require('mongoose');


var requetes = new mongoose.Schema({
	description: String,
	objet: String,
	etudiant: { type:mongoose.Schema.Types.ObjectId, ref: 'etudiants'},
	destinataire: [{ type:mongoose.Schema.Types.ObjectId, ref: 'responsables'}],
	motif: { type:mongoose.Schema.Types.ObjectId, ref: 'motifs'},
	ue: { type:mongoose.Schema.Types.ObjectId, ref: 'ues'},
	pieces_jointes:[{ type:mongoose.Schema.Types.ObjectId, ref: 'galleries'}],
	status: { type:mongoose.Schema.Types.ObjectId, ref: 'statusrequetes'},
	created_at: { type: Date, default: Date.now},
	updated_at: { type: Date, default: Date.now},
	deleted_at: { type: Date, default: Date.now},

});

module.exports = mongoose.model('requetes',requetes);